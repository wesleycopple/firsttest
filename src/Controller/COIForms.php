<?php

namespace Drupal\aace_caf\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Markup;

class COIForms extends ControllerBase {

	public function MainPage($type){

        switch ($type) {
        	case 'college':
				$myForm = $this->formBuilder()->getForm('Drupal\aace_caf\Form\CollegeForm');
				break;
			case 'executive':
				$myForm = $this->formBuilder()->getForm('Drupal\aace_caf\Form\ExecutiveForm');
				break;
			default:
				$myForm = $this->formBuilder()->getForm('Drupal\aace_caf\Form\CafForm');
				break;
		}
        $renderer = \Drupal::service('renderer');
        $myFormHtml = $renderer->render($myForm);
        $markup = Markup::create("
                <div id='mapContainer'>
                <div id='mapContent'>Here comes the form!</div>
                <div id='mapForm'>{$myFormHtml}</div>
                </div>
                
                ");

        return [
            '#markup' => $markup,
            '#allowed_tags' => ['script'],
          //   '#attached' => [
        		// 'library' => [
          // 			'aace_caf/aace_caf',
        		// 	],
        		// ],            
        ];


	}
	public function NextPage(){
		$markup = Markup::create("
                <div id='mapContainer'>
                <div id='mapContent'>Executive</div>
                <div id='mapForm'></div>
                </div>
                
                ");

        return [
            '#markup' => $markup,
            '#allowed_tags' => ['script'],
            
        ];
		
	}


}