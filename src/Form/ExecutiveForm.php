<?php
/**
 * @file
 * Contains \Drupal\aace_find_an_endo\Form\FindAnEndoForm.
 */

namespace Drupal\aace_caf\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Component\Utility\UrlHelper;


/**
 * FindAnEndoForm form.
 */
class ExecutiveForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
  	return 'executive_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {


     $content='<p>In order to actively serve on any AACE council/committee/task force, you <span style="font-weight:bold;text-decoration:underline;">must</span> fill out the three (3) forms below.
  Please click the tab, read each form carefully to make sure you know what each form entails, complete and <span style="font-weight:bold;text-decoration:underline;">submit</submit>.</p>
  <p>At each meeting/conference call you participate in during the Association year, you will be requested to submit an updated "<span style="font-weight:bold;">Multiplicity of Interest</span>"
  form in the event you have a new interest to report.</p>
  <p>By completing the forms, you are confirming your willingness to accept your appointment for every council/committee/task force listed in your appointment letter.</p>
  <p>If you have any questions, you can reach:<a href="/user/17995/contact"> Angie Lopez</a>,<a href="/user/4969/contact">Lynn Blanco</a> or <a href="/user/15964/contact">Betty Daly</a></p>';

  $fees=array(
    'None' => 'None',
    'Less than $5,000' => 'Less than $5,000',
    '$5,000-$10,000' => '$5,000-$10,000',
    '$10,000-$25,000' => '$10,000-$25,000',
    'Greater than $25,000' => 'Greater than $25,000',
  );
  $moi_options=array(
    '0'=>'To my knowledge, I, or my spouse or other familial or significant relationships (including:  parents, siblings, children, grandchildren or significant other),
      <strong>DO NOT HAVE</strong> a financial or other relationship with a medically related entity, professional association or governmental body which may
      pose a multiplicity of interest.',
    '1'=>'To my knowledge, I, or my spouse or other familial or significant relationships (including:  parents, siblings, children, grandchildren or significant other),
      <strong>DO HAVE</strong> a financial or other relationship with a medically related entity, professional association or governmental body, which may
      pose multiplicity of interest and warrants disclosure.'
  );
  $form['#attributes']=array('class' => 'hrz-tabs');

  $form['head'] = array(
   '#type' => 'item',
   '#markup' => $content,
  );

  $form['vtabs'] = array(
    '#type' => 'vertical_tabs',
  );
  $form['tab_moi'] = array(
    '#type' => 'fieldset',
    '#title' => t('Multiplicity of Interest'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'vtabs'
  );
  $form['tab_ipw'] = array(
    '#type' => 'fieldset',
    '#title' => t('Intellectual Property Waiver'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'vtabs'
  );
  $form['tab_itp'] = array(
    '#type' => 'fieldset',
    '#title' => t('Travel Policy'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'vtabs'
  );
  $form['tab_moi']['item1'] = array(
    '#markup' => '<p style="text-align:center;font-weight:bold;">BOARD/COMMITTEE/TASK FORCE MULTIPLICITY OF INTEREST FORM</p>
    <p style="text-align:center;font-weight:bold;">Background</p>
    <p>The many professional roles of endocrinologists result in a <span style="font-style:oblique;text-decoration:underline">multiplicity of interests</span>. Endocrinologists not only provide patient care, they also engage in education,
    community service, research, and other professional activities. Additionally, many endocrinologists have working relationships with third parties other than patients.</p>
    <p><span style="font-style:oblique;text-decoration:underline">Multiplicity of interests</span> could have effects on the integrity of the Association and must be disclosed so that
    this potential is made transparent. In some situations, a relationship with another organization may alter a board,  committee, or task force member\'s perspective, without any
    awareness by that individual of a loss of objectivity. Multiplicity of interests may influence an individual\'s actions in carrying out his/her responsibilities in a forthright
    and objective manner on behalf of AACE.</p>
    <p style="text-align:center;font-weight:bold;">Definition of Multiplicity of Interest</p>
    <p>Multiplicity of interests arises when a Board, Committee, or Task Force member, or his/her immediate family, employer or institution:</p>
    <ul>
      <li>is an officer, board member, trustee or owner of any medically related entity, professional association or governmental  body, or</li>
      <li>holds, under personal supervision, ownership of a stock or bond in a for-profit medically related entity, or</li>
      <li>directs institutional research funded by a medically related entity, professional association or governmental body, or</li>
      <li>receives honoraria or educational grants from a medically related entity, professional association or governmental body, or</li>
      <li>receives consulting fees from a medically related entity, professional association or governmental body, or</li>
      <li>has a direct or indirect relationship, financial or otherwise, in a medically related entity, professional association or governmental body or</li>
      <li>has a direct or indirect interest or relationship that is or may be inconsistent with or prevents the person from carrying out or otherwise fulfilling the responsibilities and duties in a manner involving AACE, or</li>
      <li>pursues interests which may influence his/her ability to exercise independent judgment in any action affecting AACE.</li>
    </ul>
    <p>Time frame: Disclosures are required for the preceding 12 months and for any new relationships which may occur during the Board/Committee/Task Force current member\'s term.</p>
    <p>Board/Committee/Task Force members have a responsibility to recognize situations where a multiplicity of interest may arise. It is the responsibility of all Board/committee/task
    force members to identify situations which may lead others to question a multiplicity of interest. Board/Committee/Task Force members have the responsibility of executing a
    Board/Committee/Task Force Disclosure Form at the beginning of each year of their term on a Board/committee/task force and to disclose any additional activities which may arise during the
    year with respect to issues brought before the Board/committee/task force. Such potential disclosures should be listed individually on the disclosure form, including the range of remuneration
    if the relationship is of a financial nature.</p>
    <p>This form will be used for internal AACE activities only and shall not be disclosed to third parties unless required by law.</p>',
  );
  $form['tab_moi']['moi_financial'] = array(
    '#type' => 'radios',
    '#title' => 'Financial Relationship',
    '#options' => $moi_options,
    '#default_value' => isset($data['moi_financial'])?$data['moi_financial']:NULL,
    '#required' => TRUE,
  );
  $form['tab_moi']['field_consult_fee'] = array(
    '#type' => 'fieldset',
    '#title' => t('CONSULTING FEES:'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#prefix' => '<div style="font-weight:bold;background-color:#F4F4F4;">',
    '#suffix' => '</div>',
  );
  $form['tab_moi']['field_consult_fee']['consulting_amount'] = array(
    '#type' => 'select',
    '#title' => 'Fee Amount:',
    '#options' => $fees,
    '#default_value' => ((isset($data['consulting_amount']))?$data['consulting_amount']:'None'),
    '#prefix' => '<div class="alignform">',
    '#suffix' => '</div>',
  );
  $form['tab_moi']['field_consult_fee']['consulting_company'] = array(
    '#type' => 'textfield',
    '#title' => 'Name of Company:',
    '#size' => 55,
    '#default_value' => ((isset($data['consulting_company']))?$data['consulting_company']:''),
    '#prefix' => '<div class="alignform">',
    '#suffix' => '</div><div style="clear:both;"></div>',
  );
  $form['tab_moi']['field_consult_fee']['consulting_additional'] = array(
    '#type' => 'textarea',
    '#default_value' => ((isset($data['consulting_additional']))?$data['consulting_additional']:''),
    '#title' => 'If more than one company, please list companies and amounts:',
  );
  $form['tab_moi']['field_speaker_fee'] = array(
    '#type' => 'fieldset',
    '#title' => t('SPEAKER FEES:'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#prefix' => '<div style="font-weight:bold;background-color:#F4F4F4;">',
    '#suffix' => '</div>',
  );
  $form['tab_moi']['field_speaker_fee']['speaker_amount'] = array(
    '#type' => 'select',
    '#title' => 'Fee Amount:',
    '#options' => $fees,
    '#default_value' => ((isset($data['speaker_amount']))?$data['speaker_amount']:'None'),
    '#prefix' => '<div class="alignform">',
    '#suffix' => '</div>',
  );
  $form['tab_moi']['field_speaker_fee']['speaker_company'] = array(
    '#type' => 'textfield',
    '#title' => 'Name of Company:',
    '#size' => 55,
    '#default_value' => ((isset($data['speaker_company']))?$data['speaker_company']:''),
    '#prefix' => '<div class="alignform">',
    '#suffix' => '</div><div style="clear:both;"></div>',
  );
  $form['tab_moi']['field_speaker_fee']['speaker_additional'] = array(
    '#type' => 'textarea',
    '#default_value' => ((isset($data['speaker_additional']))?$data['speaker_additional']:''),
    '#title' => 'If more than one company, please list companies and amounts:',
  );
  $form['tab_moi']['field_stock_own'] = array(
    '#type' => 'fieldset',
    '#title' => t('STOCK OWNERSHIP:'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#prefix' => '<div style="font-weight:bold;background-color:#F4F4F4;">',
    '#suffix' => '</div>',
  );
  $form['tab_moi']['field_stock_own']['stock_amount'] = array(
    '#type' => 'select',
    '#title' => 'Stock Amount:',
    '#options' => $fees,
    '#default_value' => ((isset($data['stock_amount']))?$data['stock_amount']:'None'),
    '#prefix' => '<div class="alignform">',
    '#suffix' => '</div>',
  );
  $form['tab_moi']['field_stock_own']['stock_company'] = array(
    '#type' => 'textfield',
    '#title' => 'Name of Company:',
    '#size' => 55,
    '#default_value' => ((isset($data['stock_company']))?$data['stock_company']:''),
    '#prefix' => '<div class="alignform">',
    '#suffix' => '</div><div style="clear:both;"></div>',
  );
  $form['tab_moi']['field_stock_own']['stock_additional'] = array(
    '#type' => 'textarea',
    '#default_value' => ((isset($data['stock_additional']))?$data['stock_additional']:''),
    '#title' => 'If more than one company, please list companies and amounts:',
  );
  $form['tab_moi']['field_research_grant'] = array(
    '#type' => 'fieldset',
    '#title' => t('RESEARCH GRANTS:'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#prefix' => '<div style="font-weight:bold;background-color:#F4F4F4;">',
    '#suffix' => '</div>',
  );
  $form['tab_moi']['field_research_grant']['research_amount'] = array(
    '#type' => 'select',
    '#title' => 'Grant Amount:',
    '#options' => $fees,
    '#default_value' => ((isset($data['research_amount']))?$data['research_amount']:'None'),
    '#prefix' => '<div class="alignform">',
    '#suffix' => '</div>',
  );
  $form['tab_moi']['field_research_grant']['research_company'] = array(
    '#type' => 'textfield',
    '#title' => 'Name of Company:',
    '#size' => 55,
    '#default_value' => ((isset($data['research_company']))?$data['research_company']:''),
    '#prefix' => '<div class="alignform">',
    '#suffix' => '</div><div style="clear:both;"></div>',
  );
  $form['tab_moi']['field_research_grant']['research_additional'] = array(
    '#type' => 'textarea',
    '#default_value' => ((isset($data['research_additional']))?$data['research_additional']:''),
    '#title' => 'If more than one company, please list companies and amounts:',
  );
  $form['tab_moi']['field_non_financial'] = array(
    '#type' => 'fieldset',
    '#title' => t('NON FINANCIAL ACTIVITIES:'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#prefix' => '<div style="font-weight:bold;background-color:#F4F4F4;">',
    '#suffix' => '</div>',
  );
  $form['tab_moi']['field_non_financial']['non_financial_company'] = array(
    '#type' => 'textfield',
    '#title' => 'Name of Company or Organization:',
    '#size' => 55,'#default_value' => ((isset($data['non_financial_company']))?$data['non_financial_company']:''),
    '#prefix' => '<div class="alignform">',
    '#suffix' => '</div><div style="clear:both;"></div>',
  );
  $form['tab_moi']['field_non_financial']['non_financial_additional'] = array(
    '#type' => 'textarea',
    '#default_value' => ((isset($data['non_financial_additional']))?$data['non_financial_additional']:''),
    '#title' => 'If more than one company, please list companies and activities:',
  );
  $form['tab_moi']['moi_agree'] = array(
    '#markup' => '<p>By submitting this form, I, '.$profile->first_name.' '.$profile->last_name.
    ', agree to notify AACE if there are any changes to this disclosure form which occur during my term of service on the board/committee(s)/task force(s).</p>',
  );
  $form['tab_moi']['moi_next'] = array(
    '#markup' => '<a id="preceed" href="#">Please proceed to the next tab</a>',
    '#prefix' => '<div style="text-align:center;font-weight:bold;font-size:1.2em;">',
    '#suffix' => '</div>',
  );
  $form['tab_ipw']['ipw_content'] = array(
    '#markup' => '<p style="text-align:center;font-weight:bold;">American Association of Clinical Endocrinologists (AACE)
      <br/>Waiver of Intellectual Property and Confidentiality Agreement</p>
      <p style="text-align:center;text-decoration:underline;">BACKGROUND</p>
      <p>AACE members work together on common issues in sharing and offering personal ideas through their service on the AACE Board of Directors, Committees and Task Forces in
      order to achieve specific goals and objectives (e.g., initiatives, guidelines, patient education brochures, and the creation and development of member benefits). Under the direction or
      charge of the Board, Committees, or Task Forces, AACE members will agree to waive any and all proprietary rights to personal intellectual property when initiated as part of
      an AACE group process.  This waiver is necessitated through AACE\'s own Articles of Incorporation, which state:</p>
      <p style="text-align:center;font-weight:bold;font-style:oblique;">AACE Articles of Incorporation<br/>Article V: Powers, Section (a)</p>
      <p style="font-style:oblique;">"No part of the income, profit or assets of the corporation shall inure to the benefit of, or be distributable to, directly or indirectly, its members,
      directors, officers, or other private persons; provided, however, that the corporation shall be authorized and empowered to pay reasonable compensation for services rendered and to
      make payments and distributions in furtherance of the purposes set forth in Article III: Purposes - ...The specific purpose for which this corporation is organized is to ensure optimal
      care and the highest standard of medical practice for patients with endocrine disease and related disorders and to serve as a national representative spokesman for and to the physicians
      treating patients with endocrine disease and related disorders, and thereby to serve as a resource for the development of national policy concerning endocrine disease."</p>
      <p>In light of this requirement, the AACE Board of Directors adopted official policy on November 6, 1999, to assist in clarifying the issue of intellectual property ownership as indicated below:</p>
      <p style="text-align:center;font-weight:bold;">AACE Policy Re Intellectual Property<br/>Adopted as Policy November 6, 1999</p>
      <p>"That AACE recognizes the right of its members to own intellectual property and respects the rights of those members to establish the conditions under which AACE may share in
      the use of their property. If those conditions cannot be amicably agreed to in writing by both parties, AACE will not pursue further interests in the use of said property and will
      cease and desist in any future use, publication of, or allusion to, the property in question; and further, that intellectual property which is initiated by an individual remains the
      property of the individual and that intellectual property which is initiated as part of an AACE group process remains the property of AACE."</p>
      <p style="text-align:center;font-weight:bold;text-decoration:underline;">Use of Proprietary Information</p>
      <p>That representation by a member, serving on the AACE Board, Committees, or Task Forces, who may be a member of or serve in a similar position in another endocrine-related organization,
      often has positive consequences through the cross pollination of ideas which may serve to benefit both organizations in a joint or collaborative effort. However, one organization may develop
      or consider information that could be deemed as proprietary and should never be discussed with another organization. AACE members acknowledge that he or she may be granted access to
      information that may be proprietary (defined as information <span style="font-weight:bold;">derived from the AACE Board, Committees, or Task Forces</span> not otherwise available in the
      public domain and which was previously unknown by the member) to AACE programs or activities. AACE Board, Committee, or Task Force members agree to protect the confidentiality of such
      information as may be identified by the associated Chair and that such information not be shared with other entities without the express written consent of AACE.</p>
      <p style="text-align:center;font-weight:bold;font-style:oblique;padding:5px 0px;border:3px solid black;">AACE Board,Committee, and Task Force members are required to read and
      agree to the<br/>policies set forth above in order to participate in AACE activities.</p>
      <p>By submitting this form, I, '.$profile->first_name.' '.$profile->last_name.', hereby agree to adhere to AACE policy regarding intellectual property as stated above.
      I also understand that this Waiver will extend through the expiration/termination of my Board,Committee(s) or Task Force(s) assignments on behalf of AACE.
      <span style="font-weight:bold;">This agreement shall survive the member\'s service on the AACE Board or any AACECommittee(s) or Task Force(s).</span></p>',
  );
  $form['tab_ipw']['ipw_next'] = array(
    '#markup' => '<a id="preceed" href="#">Please proceed to the next tab</a>',
    '#prefix' => '<div style="text-align:center;font-weight:bold;font-size:1.2em;">',
    '#suffix' => '</div>',
  );
  $form['tab_itp']['item1'] = array(
    '#markup' => '<center><strong>American Association of Clinical Endocrinologists (AACE)<br/>Travel Policy<br/>Association Year 2015-2016</strong></center>
    <p>AACE makes every reasonable effort to protect the safety and welfare of its representatives during the course of their participation in both domestic and international meetings on behalf of AACE.</p>
    <p>AACE provides travel accident and dismemberment coverage at no cost for all members traveling to domestic and international locations on behalf of AACE in the amounts:</p>
    <ul>
     <ol>Domestic – $150,000 for each individual and $1.5 million aggregate (any one incident for all insured persons).</ol>
     <ol>International – $175,000 for each individual and $1.75 million aggregate limit (any one accident) for all insured persons.</ol>
    </ul>
    <p>In the event that the US State Department issues a travel warning for the country/region that a meeting is being held, AACE cannot participate in the meeting and risk the safety and welfare of its members and/or staff.  If after agreeing to co-sponsor a meeting with an organization within a country/region at which AACE members and/or staff are officially present on behalf of AACE and for which a travel warning to that meeting location has been issued by the US State Department, AACE will withdraw its participation at any time.  In the event a travel warning has already been issued, AACE will advise the co-sponsoring organization at least two (2) months prior to the date of the event if it becomes necessary to withdraw its official participation based on travel safety issues and will also advise affected AACE representatives of this action.  This policy does not prevent any AACE member from traveling to the meeting as an individual at their own risk.</p>
    <p>By submitting this form, I, '.$profile->first_name.' '.$profile->last_name.', have read and acknowledge the AACE policy regarding international travel. I also understand that
    this policy will extend through the expiration/termination of my Board,Committee(s) or Task Force(s) assignments on behalf of AACE.
    <span style="font-weight:bold;">This policy shall survive the member\'s service on the AACE Board or any AACE, committee(s) or task force(s).</span></p>',
  );
  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $user->uid,
  );
  $form['nat'] = array(
    '#type' => 'hidden',
    '#value' => $profile->first_name.' '.$profile->last_name,
  );
  $form['tab_itp']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit Forms',
    '#prefix' => '<center>',
    '#suffix' => '</center>',
  );
  return $form;
  }



  

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      drupal_set_message($key . ': ' . $value);
    }
  }

}