<?php
/**
 * @file
 * Contains \Drupal\aace_find_an_endo\Form\FindAnEndoForm.
 */

namespace Drupal\aace_caf\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Component\Utility\UrlHelper;


/**
 * FindAnEndoForm form.
 */
class CafForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
  	return 'caf_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {


	$form['information'] = array(
	  '#type' => 'horizontal_tabs',
	  '#default_tab' => 'edit-publication',
	);
	$form['author'] = array(
	  '#type' => 'details',
	  '#title' => $this
	    ->t('Author'),
	  '#group' => 'information',
	);
	$form['author']['name'] = array(
	  '#type' => 'textfield',
	  '#title' => $this
	    ->t('Name'),
	);

	$form['publication'] = array(
	  '#type' => 'details',
	  '#title' => $this
	    ->t('Publication'),
	  '#group' => 'information',
	);
	$form['publication']['publisher'] = array(
	  '#type' => 'textfield',
	  '#title' => $this
	    ->t('Publisher'),
	);
        $form['submit'] = [
            '#type' => 'submit',
            '#value' => t('Save'),
        ];
        return $form; 
  }



  

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      drupal_set_message($key . ': ' . $value);
    }
  }

}